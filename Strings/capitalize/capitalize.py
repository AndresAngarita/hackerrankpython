def solve(s):
    if len(s)>0 and len(s)<1000:
        text = ""
        for i in range(len(s)):
            if i>0:
                if s[i-1] == " " and s[i].isalpha():
                    text += s[i].upper()    
                else:
                    text += s[i]
            else:
                text += s[i].upper()
            
        return text

if __name__ == '__main__':
    s = input()
    result = solve(s)
    print(result)
