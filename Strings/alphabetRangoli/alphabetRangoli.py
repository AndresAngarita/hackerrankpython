alpha = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
         
def searchLetter(patron):
    newPatron = ""
    patron = str(patron)
    
    for i in range(len(patron)):
        if patron[i].isalpha():
            for j in range(len(alpha)):
                if patron[i]==alpha[j]:
                    newPatron += alpha[j-1]
        else:
            newPatron += '-'    
    
    return newPatron

def print_rangoli(n):
    if n>0 and n<27:
        pos = n-1
        patron = alpha[pos]
        invPatron = ""
        for i in range(n):
            if i < n-1:
                invPatron += patron.center(((4*n)-3),"-")+"\n" 
                print (patron.center(((4*n)-3),"-"))
                patron = alpha[n-1]+"-"+searchLetter(patron)+"-"+alpha[n-1]
            else:
                print (patron.center(((4*n)-3),"-"),end = "")
                patron = alpha[n-1]+"-"+searchLetter(patron)+"-"+alpha[n-1]
                print(invPatron[::-1])

if __name__ == '__main__':
    n = int(input())
    print_rangoli(n)            