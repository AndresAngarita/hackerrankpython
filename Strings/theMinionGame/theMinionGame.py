def minion_game(string):
    vocals = ['A','E','I','O','U']
    tamString = 0 
    kevin = 0
    stuart = 0
    for i in string:
        tamString += 1
    if tamString > 0 and tamString <= 10**6:
        for l in range(tamString):
            if string[l].isalpha():
                if string[l].isupper():
                    if vocals.count(string[l])>0:
                        kevin += tamString-l
                    else:
                        stuart += tamString-l
    if kevin > stuart:
        print("Kevin {}".format(kevin))
    elif kevin < stuart:
        print("Stuart {}".format(stuart))
    elif kevin == stuart:
        print("Draw") 
    
if __name__ == '__main__':
    s = input()
    minion_game(s)