def merge_the_tools(string, k):
    if len(string)<=10000 and len(string)>=1:
        if k <= len(string) and k>=1:
            tam = int(len(string)/k)
            
            for i in range(tam):
                print(getNotRepeated(string[i*k:(i*k)+k]))
            
def getNotRepeated(substri):
    listSub = list(substri)
    listNew = []
    
    for i in listSub:
        if i not in listNew:
            listNew.append(i)
    
    return "".join(listNew)

if __name__ == '__main__':
    string, k = input(), int(input())
    merge_the_tools(string, k)