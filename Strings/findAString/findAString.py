def count_substring(string, sub_string):
    count = 0
    for i in range(len(string)):
        if i > (len(string)-len(sub_string)):
            break
        if string[i] == sub_string[0]:
            letters = 0
            for j in range(len(sub_string)):
                if string[i+j] == sub_string[j]:
                    letters += 1
            if letters == len(sub_string):
                count += 1    
    return count

if __name__ == '__main__':
    string = input().strip()
    sub_string = input().strip()
    
    count = count_substring(string, sub_string)
    print(count)