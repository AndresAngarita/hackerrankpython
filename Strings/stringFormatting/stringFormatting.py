n = int(input())
res = ""
def changeBase(number,base,res):
    hex =[0,1,2,3,4,5,6,7,8,9,"A","B","C","D","E","F"]
    if number<base:
        if base == 16:
            res += str(hex[number])
        else:
            res += str(number)
        return res[::-1]
    else:
        if base == 16:
            res += str(hex[(number%base)])
        else:
            res += str(number%base)
        number = number//base
        return changeBase(number,base,res)         


tamChangeBase = len(changeBase(n,2,res))

for i in range(1, n+1):
    print(changeBase(i,10,res).rjust(tamChangeBase," "),changeBase(i,8,res).rjust(tamChangeBase," "),changeBase(i,16,res).rjust(tamChangeBase," "),changeBase(i,2,res).rjust(tamChangeBase," "))