def is_leap(year):
    leap = False
    if year>=1900 and year<=(pow(10,5)):
        if (year%4) == 0:
            if (year%100) == 0:
                if (year%400) == 0:
                    return True
                return False
            return True    
    return leap
year = int(input())
print(is_leap(year))    