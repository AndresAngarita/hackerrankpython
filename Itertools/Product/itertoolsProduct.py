from itertools import product
from sys import stdin,stdout

inp1 = stdin.readline().split(" ")
inp2 = stdin.readline().split(" ")
list1 = [int(i) for i in inp1]
list2 = [int(i) for i in inp2]

if len(list1)<30 and len(list2)<30:
    result = list(product(list1,list2))    
    for tupli in result:
        stdout.write(str(tupli)+" ")