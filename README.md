# Solutions for python exercises proposed by HackerRank

In this repository you will find a solution proposal for some of the exercises found in hackerrank (a technology company that focuses on the challenges of competitive programming), I hope it is useful for you.

Within each folder there is a file with the description of the problem downloaded from the official page, and the .py file with my solution proposal, feel free to clone or download the files and edit them.

# Solución de ejercicios de python propuestos por HackerRank

En este repositorio encontrará una propuesta de solución para algunos de los ejercicios que se encuentran en hackerrank (una empresa de tecnología que se centra en los desafíos de la programación competitiva), espero que sean de utilidad para usted.

Dentro de cada carpeta hay un archivo con la descripción del problema descargado de la página oficial, y el archivo .py con mi propuesta de solución, siéntase libre de clonar o descargar los archivos y editarlos.