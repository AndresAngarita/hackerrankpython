if __name__ == '__main__':
    n = int(input())
    student_marks = {}
    if n>=2 and n<=10:
        for _ in range(n):
            data = input().split()
            scores = list(map(float, data[1:4]))
            validateRange = 0
            for i in range(len(scores)):
                if scores[i]<0 or scores[i]>100:
                    validateRange += 1
            if validateRange == 0:
                student_marks[data[0]] = scores
        query_name = input()

        if query_name in student_marks:
            scores = student_marks[query_name]
            promedio = 0
            for i in range(len(scores)):
                promedio += scores[i]
            promedio = promedio/(len(scores))    
        print("%.2f" % promedio)
